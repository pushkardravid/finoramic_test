class Solution:

    def pow(self, x, n, d):
        if x==0:
            return 0
        elif n==0:
            return 1
        t = pow(x, int(n/2),d)
        if n%2==0:
            return (t*t)%d
        else:
            if n>0:
                return (x*t*t)%d
            else:
                return ((t*t)/x)%d