class Solution:

    def getNumbers(self, row):
        return map(int, filter(lambda x: x.isdigit(), row))

    def getSubMatrix(self, x, y, mrx):
        return [mrx[x][y], mrx[x + 1][y], mrx[x + 2][y], mrx[x][y + 1], mrx[x + 1][y + 1], mrx[x + 2][y + 1],
                mrx[x][y + 2], mrx[x + 1][y + 2], mrx[x + 2][y + 2]]

    def isValidSudoku(self, A):
        mrx = [list(i) for i in A]
        mrxT = zip(*mrx)

        flg = True
        for row in mrx:
            numbers = self.getNumbers(row)
            if len(numbers) != len(set(numbers)) or len(filter(lambda x: x < 1 or x > 9, numbers)) > 0:
                flg = False
                break
        if flg:
            for col in mrxT:
                numbers = self.getNumbers(list(col))
                if len(numbers) != len(set(numbers)) or len(filter(lambda x: x < 1 or x > 9, numbers)) > 0:
                    flg = False
                    break

        if flg:
            for i in range(0, 9, 3):
                for j in range(0, 9, 3):
                    numbers = self.getNumbers(self.getSubMatrix(i, j, mrx))
                    if len(numbers) != len(set(numbers)) or len(filter(lambda x: x < 1 or x > 9, numbers)) > 0:
                        flg = False
                        break
        if flg:
            return 1
        else:
            return 0
