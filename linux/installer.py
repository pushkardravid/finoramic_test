import json,sys,os,subprocess

print "-----------------Installation started-----------------"
FNULL = open(os.devnull, 'w')
failed_packages = []
failed_status = False

try:
    subprocess.call("apt-get install python-pip", shell=True, stdout=FNULL, stderr=subprocess.STDOUT)
    file = open(sys.argv[1])
    data = json.load(file)
    dependencies = data["Dependencies"]
    for dependency in dependencies:
        cmd = "pip install " + dependency
        install_status = subprocess.call(cmd, shell=True, stdout=FNULL, stderr=subprocess.STDOUT)
        if install_status:
            failed_status = True
            failed_packages.append(dependency)
    if failed_status:
        print "The following packages were not installed:"
        print '\n'.join(failed_packages)
    else:
        print "success"

except Exception as e:
    print str(e) + "\n" + "Sample usage: python pinstall.py <requirements JSON file>"

print "----------------Installation completed----------------"