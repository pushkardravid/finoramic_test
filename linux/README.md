#Python package installer

A simple python package installer that uses pip for downloading packages.

### Usage

```
python installer.py <requirementsfile.json>
```
